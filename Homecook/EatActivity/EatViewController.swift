//
//  EatViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 1/7/19.
//  Copyright © 2019 Ahmed Bahaa. All rights reserved.
//

import UIKit

class EatViewController: UIViewController {

    @IBOutlet weak var menuBtn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        menuBtn.target = revealViewController()
        menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
    }
}
