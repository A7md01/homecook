//
//  SplashViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/15/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//

import UIKit
import SVProgressHUD
class SplashViewController: UIViewController {
    @IBOutlet weak var exitButton: UIButton!
    @IBAction func ExitAction(_ sender: Any) {
        exit(0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.exitButton.isHidden = true
        SVProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+2) {
            if ReachabilityTest.isConnectedToNetwork() {
                SVProgressHUD.dismiss()
                print("Internet connection available")
                self.performSegue(withIdentifier: "nextPage", sender: nil)
                
            }
            else{
                
                self.ShowAlert(title: "No Internet Connection", message: "Internet connection is needed to proced . please connect then try again")
                print("No internet connection available")
                self.exitButton.isHidden = false
                
                SVProgressHUD.dismiss()
            }
        }
    }
}
