//
//  RestrictionsCollectionViewCell.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 1/1/19.
//  Copyright © 2019 Ahmed Bahaa. All rights reserved.
//

import UIKit

class RestrictionsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var restrictionImg: UIImageView!
    @IBOutlet weak var restrictoinName: UILabel!
}
