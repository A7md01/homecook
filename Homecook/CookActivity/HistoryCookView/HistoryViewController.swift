//
//  HistoryViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 1/7/19.
//  Copyright © 2019 Ahmed Bahaa. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    @IBOutlet weak var btnMenu: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnMenu.target = revealViewController()
        btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))

        // Do any additional setup after loading the view.
    }

}
