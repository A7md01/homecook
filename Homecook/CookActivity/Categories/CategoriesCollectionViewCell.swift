//
//  CategoriesCollectionViewCell.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 1/2/19.
//  Copyright © 2019 Ahmed Bahaa. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var CatName: UILabel!
    @IBOutlet weak var CatImage: UIImageView!
}
