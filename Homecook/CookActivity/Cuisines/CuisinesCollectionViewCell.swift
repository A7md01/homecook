//
//  CuisinesCollectionViewCell.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/30/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//

import UIKit

class CuisinesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameImg: UILabel!
}
