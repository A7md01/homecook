//
//  ViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/29/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//

import UIKit
import Firebase

class CoViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate , UICollectionViewDelegate , UICollectionViewDataSource ,UITextFieldDelegate{
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var cuisinesCollectionView: UICollectionView!
    @IBOutlet weak var restrictionsCollectionView: UICollectionView!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var dishNameTF: UITextField!
    @IBOutlet weak var dishDescriptionTF: UITextField!
    @IBOutlet weak var numberofServingsTF: UITextField!
    @IBOutlet weak var priceOfServingTF: UITextField!
    @IBOutlet weak var camera: UIButton!
    @IBOutlet weak var image: UIButton!
    @IBOutlet weak var bin: UIButton!
    @IBOutlet weak var cookImage: UIImageView!
    @IBOutlet weak var pickupAddress: UITextField!
    @IBOutlet weak var pickupApartment: UITextField!
    @IBOutlet weak var pickupBuzzer: UITextField!
    
    
    var CuisinesImg = [UIImage(named : "african")!,UIImage(named : "american")!,UIImage(named : "arabian")!,UIImage(named : "canadian")!,UIImage(named : "caribbean")!,UIImage(named : "chinese")!,UIImage(named : "egyptian")!,UIImage(named : "french")!,UIImage(named : "greek")!,UIImage(named : "indian")!,UIImage(named : "italian")!,UIImage(named : "japanese")!,UIImage(named : "lebanese")!,UIImage(named : "mexican")!,UIImage(named : "moroccan")!,UIImage(named : "persian")!,UIImage(named : "spanish")!,UIImage(named : "turkish")!]
    var CuisinesName = ["African" , "American" , "Arabian","Canadian", "Caribbean","Chinese", "Egyptian" , "French","Greek", "Indian","Italian" ,"Japanese" ,"Lebanese", "Mexican" ,"Moroccan" , "Persian" , "Spanish" ,"Turkish" ]
    
    var restImg = [UIImage(named : "halal")!,UIImage(named : "kosher")!,UIImage(named : "vegan")!,UIImage(named : "veggie")!]
    var restName = ["Halal","Kosher","Vegan","Veggie"]
    var catImg = [UIImage(named : "baking")!,UIImage(named : "dessert")!,UIImage(named : "meat")!,UIImage(named : "poultry")!,UIImage(named : "salad")!,UIImage(named : "sandwich")!,UIImage(named : "seafood")!]
    var catName = ["Baking" , "Dessert" , "Meat" , "Poultry" , "Salad" , "Sandwich" , "Seafood"]
    override func viewDidLoad() {
        super.viewDidLoad()
        menu.target = revealViewController()
        revealViewController().rearViewRevealWidth = 280
        menu.action = #selector(SWRevealViewController.revealToggle(_:))
        cuisinesCollectionView.delegate = self
        cuisinesCollectionView.dataSource = self
        restrictionsCollectionView.delegate = self
        restrictionsCollectionView.dataSource = self
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        imagePicker.delegate = self
        dishNameTF.underlined()
        dishDescriptionTF.underlined()
        numberofServingsTF.underlined()
        priceOfServingTF.underlined()
        pickupAddress.underlined()
        pickupApartment.underlined()
        pickupBuzzer.underlined()
        dishNameTF.addTarget(self, action: #selector(myTargetFunction), for: UIControl.Event.touchUpInside)
    }
    @IBAction func BinAction(_ sender: UIButton) {
        cookImage.image = nil
        return
            cookImage.image = UIImage(named: "photo_your_food")
         present(imagePicker, animated: false , completion: nil)
    }
    @IBAction func ImageAction(_ sender: UIButton) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker , animated: true , completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            cookImage.image = image
        }
        dismiss(animated: true, completion: nil)
    }
    @IBAction func CameraAction(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func addPickerTimeAction(_ sender: UIButton) {
        print("sfhsdjfhsdjhfjsdhfjsdfgsavdgashdvgsahdvghasv")
    }
    
    @IBAction func PostAction(_ sender: UIButton) {
    }
    
    @IBAction func CancelAction(_ sender: UIButton) {
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.cuisinesCollectionView {
            return CuisinesImg.count
        }else if collectionView == self.restrictionsCollectionView
        {
            return restImg.count
        }else{
            return catImg.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.cuisinesCollectionView{
            let cell:CuisinesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cuisinescell", for: indexPath) as! CuisinesCollectionViewCell
            cell.nameImg.text! = CuisinesName[indexPath.row]
            cell.image.image = CuisinesImg[indexPath.row]
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 0.5
            return cell
        }else if collectionView == self.restrictionsCollectionView{
            let cell:RestrictionsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RestrictionsCell", for: indexPath) as! RestrictionsCollectionViewCell
            cell.restrictoinName.text! = restName[indexPath.row]
            cell.restrictionImg.image = restImg[indexPath.row]
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 0.5
            return cell
        }else{
            let cell:CategoriesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCollectionViewCell
            cell.CatName.text! = catName[indexPath.row]
            cell.CatImage.image = catImg[indexPath.row]
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 0.5
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.cuisinesCollectionView {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.layer.borderColor = UIColor.gray.cgColor
            cell?.layer.borderWidth = 2
            print(CuisinesName[indexPath.row])
        }else if collectionView == self.restrictionsCollectionView{
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.layer.borderColor = UIColor.gray.cgColor
            cell?.layer.borderWidth = 2
            print(restName[indexPath.row])
        }else{
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.layer.borderColor = UIColor.gray.cgColor
            cell?.layer.borderWidth = 2
            print(catName[indexPath.row])
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == self.cuisinesCollectionView {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.layer.borderColor = UIColor.lightGray.cgColor
            cell?.layer.borderWidth = 0.5
        }else {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.layer.borderColor = UIColor.lightGray.cgColor
            cell?.layer.borderWidth = 0.5
        }
    }
    @objc func myTargetFunction(textField: UITextField) {
        print("myTargetFunction")
    }
}
