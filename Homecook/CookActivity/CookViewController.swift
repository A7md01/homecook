//
//  CookViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/26/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//

import UIKit

class CookViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var dishNameTF: UITextField!
    @IBOutlet weak var dishDescriptionTF: UITextField!
    @IBOutlet weak var numberofServingsTF: UITextField!
    @IBOutlet weak var priceOfServingTF: UITextField!
    @IBOutlet weak var camera: UIButton!
    @IBOutlet weak var image: UIButton!
    @IBOutlet weak var bin: UIButton!

    @IBOutlet weak var cookImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        menuBtn.target = revealViewController()
        menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        imagePicker.delegate = self
        dishNameTF.underlined()
        dishDescriptionTF.underlined()
        numberofServingsTF.underlined()
        priceOfServingTF.underlined()
        
        
    }
    @IBAction func BinAction(_ sender: UIButton) {
        cookImage.image = nil
        return
            cookImage.image = UIImage(named: "french")
        present(imagePicker, animated: false , completion: nil)
    }
    
    @IBAction func ImageAction(_ sender: UIButton) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker , animated: true , completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            cookImage.image = image
        }
        dismiss(animated: true, completion: nil)
    }
    @IBAction func CameraAction(_ sender: UIButton) {
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        present(imagePicker , animated: true , completion: nil)
    }
    
}
