//
//  LoginViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/7/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SVProgressHUD
class LoginViewController: UIViewController {
    var window: UIWindow?
    @IBOutlet weak var labEmail: UILabel!
    @IBOutlet weak var labPassword: UILabel!
    @IBOutlet weak var loginview: UIView!
    @IBOutlet weak var userPasswordTF: UITextField!
    @IBOutlet weak var userEmailTF: UITextField!
    @IBAction func LoginBtnAction(_ sender: UIButton) {
        let email = validateEmail(enteredEmail: userEmailTF.text!)
        if userEmailTF.text == "" {
            labEmail.isHidden = false
            labEmail.text = "required"
            return
        }
        else if email == false  {
            labEmail.isHidden = false
            labEmail.text = "Invalid Format"
            return
        }else{
            labEmail.isHidden = true
        }

        if userPasswordTF.text == "" {
            labPassword.isHidden = false
            labPassword.text = "required"
            return
        }else{
            labPassword.isHidden = true
        }
        
        Auth.auth().signIn(withEmail: userEmailTF.text!, password: userPasswordTF.text!) { (user, error) in
            if let error = error{
                print(error.localizedDescription)
                if error._code == 17011{
                    self.ShowAlert(title: "User Does Not Exist", message: "User With this email not exist , please SignUp")
                }    else if error._code == 17009{
                    self.ShowAlert(title: "Invalid Password", message: "The Password you have entered does not match our records . if you forgetten your password please choose 'forgot password'")
                    
                }
            }
            else{
                SVProgressHUD.show()
                let storyBoard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MainVC") as! ModeSelectViewController
                self.present(nextViewController, animated:true, completion:nil)
                SVProgressHUD.dismiss()
            }
        }
    }
    @IBAction func ExitApp (sender : UIButton ){
        exit(0)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPaddingTF(to: userEmailTF)
        addPaddingTF(to: userPasswordTF)
        loginview.layer.cornerRadius = 5
        loginview.layer.masksToBounds = true
        labEmail.isHidden = true
        labPassword.isHidden = true
    }
}
