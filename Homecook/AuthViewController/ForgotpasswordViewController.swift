//
//  ForgotpasswordViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/7/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//

import UIKit
import Firebase

class ForgotpasswordViewController: UIViewController {
    var window: UIWindow?
    @IBOutlet weak var userEmailTF: UITextField!
    @IBOutlet weak var labEmail: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labEmail.isHidden = true
        addPaddingTF(to: userEmailTF)
    }
    @IBAction func ResetPasswordAction(_ sender: Any) {
        let email = validateEmail(enteredEmail: userEmailTF.text!)
        if userEmailTF.text == "" {
            labEmail.isHidden = false
            labEmail.text = "required"
            return
        }else{
            labEmail.isHidden = true
        }
        if email == false  {
            labEmail.isHidden = false
            labEmail.text = "Invalid Format"
            return
        }else{
            labEmail.isHidden = true
        }
        guard let mail = userEmailTF.text , mail != " " else {return}
        Auth.auth().sendPasswordReset(withEmail: mail) { (error) in
            if let error = error {
                print(error.localizedDescription)
                self.ShowAlert(title: "User Does Not Exist", message: "A user with this email does not exist , Please Sign up")
            }else{
                self.ShowAlert(title: " Reset Password", message: "An email has been sent to you. Please use it to change ypur password then try login again")
                let sb = UIStoryboard(name: "Main", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "LoginVC")
                self.window?.rootViewController = vc
                
            }
        }
        
    }
}
