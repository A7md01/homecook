//
//  SignupViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/7/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//     AIzaSyAgkhvDtZL6B13Mn36fdBhYfTHz6Su8jxM
import UIKit
import CountryPickerView
import Firebase
class SignupViewController: UIViewController {
      var window: UIWindow?
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var userEmailTF: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var userPhoneTF: UITextField!
    @IBOutlet weak var AddressSearchBar: UISearchBar!
    @IBOutlet weak var ApertmentTF: UITextField!
    @IBOutlet weak var Buzzer: UITextField!
    @IBOutlet weak var nameError: UIImageView!
    @IBOutlet weak var emailError: UIImageView!
    @IBOutlet weak var passwordError: UIImageView!
    @IBOutlet weak var phoneError: UIImageView!
    @IBOutlet weak var apartmentError: UIImageView!
    @IBOutlet weak var buzzerError: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addPaddingTF(to: fullNameTF)
        addPaddingTF(to: userEmailTF)
        addPaddingTF(to: userPassword)
        addPaddingTF(to: userPhoneTF)
        addPaddingTF(to: ApertmentTF)
        addPaddingTF(to: Buzzer)
        backView.layer.cornerRadius = 5
        backView.layer.masksToBounds = true
        let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        userPhoneTF.leftView = cpv
        userPhoneTF.leftViewMode = .always
        nameError.isHidden = true
        emailError.isHidden = true
        passwordError.isHidden = true
        phoneError.isHidden = true
        apartmentError.isHidden = true
        buzzerError.isHidden = true
    }
    @IBAction func SignUpAction(_ sender: UIButton) {
//        guard let username = fullNameTF.text , username.isEmpty else { return }
//        guard let email = userEmailTF.text , email != ""  else { return }
//        guard let password = userPassword.text , password.isEmpty else {return}
//        guard let phone = userPhoneTF.text , phone.isEmpty else {return}
//        guard let apartment = ApertmentTF.text , apartment.isEmpty else {return}
//        guard let buzzer = Buzzer.text , buzzer.isEmpty else {return}
//        guard let password = userPassword.text ,
        print("Start")
        let fullname = fullNameTF.text!
        guard let email = userEmailTF.text?.trimmingCharacters(in: CharacterSet.whitespaces) , !email.isEmpty else {
            print("Enter Correct Mail")
            return
        }
        guard let password = userPassword.text , !password.isEmpty else {
            print("enter password")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let error = error {
                self.ShowAlert(title: "Error", message: error.localizedDescription)
                print(error.localizedDescription)
            }else{
                if let user = user {
                    print(user)
                    let storyBoard = UIStoryboard(name: "Main", bundle:nil)
                    let next = storyBoard.instantiateViewController(withIdentifier: "Activition") as! ActivitionCodeViewController
                    self.present(next, animated:true, completion:nil)
                }
            }
            self.saveData(username: fullname, email: email , phone: "01123849" ,address: "sdshdshds" , apart: "23" , buzzer: 23)
            print(fullname)
            print(email)
        }
    print("end")
    }
    // , email : String , phone: String , address : String , aprt : String , buzzer : NSNumber
    func saveData(username :String , email : String  , phone : String , address : String ,apart : String , buzzer : NSNumber){
        let ref = Database.database().reference().child("users").childByAutoId()
//        let childref = ref.childByAutoId()
//        print(childref.key)
        
        ref.setValue(["name" : "username",
                           "email" : "fsdfdfdsf@fdgdf.com",
                           "phones": ["0": "+1647885565"],
                           "addresses" : ["0": "sgsfgsdgs", "1": "test"],
                           "apts": ["0" : "34324", "1": "4234"],
                           "buzzers": ["0": 2000, "1": 1000],
                           "deviceTokens": ["0": "uksdgfuksdfgsddsfdsfsd", "1": "sjbfdhfsd"],
                           "locations": ["0": ["latitude" :35.220214999999996, "longitude":-40.09331820000001],
                                         "1": ["latitude" :49.220214999999996, "longitude":90.09331820000001]],
                           "oldListings": ["0": "LV60lmpVZ8dE5132roc", "1": "-LV612wtM9_0fdhwfqGc"]]
        )
        
    }
}
