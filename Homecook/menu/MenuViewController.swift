//
//  MenuViewController.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 1/7/19.
//  Copyright © 2019 Ahmed Bahaa. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
class MenuViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    @IBOutlet weak var userName: UIButton!
    @IBOutlet weak var mangeLab: UILabel!
    var menuNamrArr :Array = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        menuNamrArr = ["Create New" , "Cook History" ,"Order History" , "Switch to Eat view" ,"Switch to Cater view" , "Feedback"]
        if let user = Auth.auth().currentUser{
            mangeLab.text! = user.email!
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNamrArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        cell.lblMenuName.text! = menuNamrArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var myCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        myCell.lblMenuName.textColor = UIColor.red
        
        let revealViewController:SWRevealViewController = self.revealViewController()
        let cell:MenuTableViewCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        if cell.lblMenuName.text! == "Create New"{
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "CoViewController") as! CoViewController
            let newFrontViewCntroller = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewCntroller, animated: true)
        }
        if cell.lblMenuName.text! == "Cook History"{
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
            let newFrontViewCntroller = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewCntroller, animated: true)
        }
        if cell.lblMenuName.text! == "Switch to Eat view"{
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "EatViewController") as! EatViewController
            let newFrontViewCntroller = UINavigationController.init(rootViewController:desController)
            revealViewController.pushFrontViewController(newFrontViewCntroller, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        var myCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        myCell.lblMenuName.textColor = UIColor.black
    }
    @IBAction func logOutAvtion(_ sender: UIButton) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            print("dfjfndgnfj")
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        let storyBoard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
}
