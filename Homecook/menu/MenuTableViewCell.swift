//
//  MenuTableViewCell.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 1/7/19.
//  Copyright © 2019 Ahmed Bahaa. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMenuName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
