//
//  Extetions.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/11/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//

import UIKit
// Alert
extension UIViewController{
    func ShowAlert(title : String , message: String , oktitle: String = "OK" , okhandler :((UIAlertAction)-> Void)? = nil) {
        let alert = UIAlertController (title: title, message: message, preferredStyle:.alert)
        alert.addAction(UIAlertAction(title: oktitle, style: .cancel, handler: okhandler))
        self.present(alert, animated: true, completion: nil)
    }
    
}
