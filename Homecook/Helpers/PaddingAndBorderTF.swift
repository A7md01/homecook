//
//  PaddingAndBorderTF.swift
//  Homecook
//
//  Created by Ahmed Bahaa on 12/11/18.
//  Copyright © 2018 Ahmed Bahaa. All rights reserved.
//

import UIKit
func addPaddingTF(to textfield: UITextField) {
    let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 2.0))
    textfield.leftView = leftView
    textfield.leftViewMode = .always
}
func addPaddingAndBorder(to textfield: UITextField) {
    textfield.layer.cornerRadius =  5
    textfield.layer.borderColor = UIColor.black.cgColor
    textfield.layer.borderWidth = 1
    let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 2.0))
    textfield.leftView = leftView
    textfield.leftViewMode = .always
}

extension UITextField {
    
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
